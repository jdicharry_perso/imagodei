#!/usr/bin/env bash
#

# Premier lancement de la box lumberjack n'a jamais été lancé
# Détecter si lumberjack est installé
if [[ ! -e ${PATH_PUBLIC}composer.json ]]; then
    echo "== Install lumberjack =="
    rm -rf ${PATH_PUBLIC}
    cd ${PATH_WWW}
    # Probleme timeout
    # ${PATH_COMPOSER}lumberjack-bedrock new public --setTimeout=3600 -vvv
    # mv -f ${WP_THEMES_PATH}${WP_THEME_LUMBERJACK} ${WP_THEMES_PATH}${WP_THEME_RENAME}
    # Replacement
    git clone https://github.com/roots/bedrock.git ${PATH_PUBLIC}
    cd ${PATH_PUBLIC}
    composer --prefer-dist require rareloop/lumberjack-core
    git clone https://github.com/Rareloop/lumberjack.git ${WP_THEMES_PATH}${WP_THEME_RENAME}
    cp ${PATH_PUBLIC}.env.example ${PATH_PUBLIC}.env
    rm -rf ${PATH_PUBLIC}.github
    rm -rf ${PATH_PUBLIC}.git
    rm -rf ${WP_THEMES_PATH}${WP_THEME_RENAME}/.github
    rm -rf ${WP_THEMES_PATH}${WP_THEME_RENAME}/.git
    echo "== Install lumberjack successful =="
fi

# Clone d'un projet déjà installé
# Détecter les composer
if [[ ! -d ${PATH_PUBLIC}vendor/ ]]; then
    # composer bedrock
    cd ${PATH_PUBLIC}
    composer --prefer-dist install
    # composer theme lumberjack
    cd ${WP_THEMES_PATH}${WP_THEME_RENAME}
    composer --prefer-dist install
fi