    
#!/bin/bash
#
# variables.inc.sh
#

# Path
# ---------------------------------------
PATH_A2_SITES_AVAILABLE="/etc/apache2/sites-available/"
PATH_COMPOSER="/home/vagrant/.config/composer/vendor/bin/"
PATH_WWW="/var/www/"
PATH_PUBLIC="/var/www/public/"
PATH_PROVISION="/var/www/provision/"
PATH_PROVISION_APACHE="${PATH_PROVISION}apache/"
PATH_VAGRANT="/home/vagrant/"

WEB_ROOT="/var/www/public/web"

WP_THEMES_PATH="${WEB_ROOT}/app/themes/"
WP_THEME_LUMBERJACK="lumberjack"
WP_THEME_RENAME="jedi-theme"

FILE_APACHE="001-lumberjack"
FILE_APACHE_CONF="${FILE_APACHE}.conf"