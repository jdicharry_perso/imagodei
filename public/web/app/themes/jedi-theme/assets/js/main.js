import $ from 'jquery';
window.jQuery = $;
window.$ = $;

import 'foundation-sites';
import 'social-share-kit/dist/js/social-share-kit.min.js';


//******************* Animations sur le menu Burger *******************
$('.js--burger').on('click', function() {
    $('.header__main-menu').addClass('header__main-menu--active');
});

$('.js--close').on('click', function() {
    $('.header__main-menu').removeClass('header__main-menu--active');
});

$(document).on('keydown', function(event) {
    if (event.key == "Escape") {
        $('.header__main-menu').removeClass('header__main-menu--active');
    }
});

// ******************* Ajax pour montrer plus d'articles *******************

function launchAjaxRequest(multiplier, countedposts, termid){

    $('.ajaxWait').show();
    // ******************* ajax request *******************
    $.ajax({
        url: $('.posts_archive--selection').data('ajax'),
        method: 'GET',
        data: {
            multiplier: multiplier,
            countedposts: countedposts,
            termid: termid,
            action: 'showNewSelection'
        },
        dataType: 'html',
        success: function (response) {
            $('.ajaxWait').hide();
            $('.posts_archive--selection').empty();
            $('.posts_archive--selection').html(response);
        }
    });
}


$('.posts_archive--selection').on('click', '.js--archive-see-more', function () {
    console.log($(this).data('termid'));
    launchAjaxRequest($(this).data('multiplier'), $(this).data('countedposts'), $(this).data('termid'));
});

// ******************* initialisation du kit de partage sur les réseaux sociaux ************
SocialShareKit.init({ forceInit: true });
