<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;
use Timber\Term;

class IndexController extends Controller
{
    public function handle()
    {
        $data = Timber::get_context();
        $data['title'] = 'Tous nos articles';
        $data['terms'] = Timber::get_terms([
            'taxonomy' => 'category',
            'hide_empty' => true
        ]);
        $data['ajax'] = get_home_url().'/ajax/archives';


        $data['posts'] = Post::query();

        if (is_category()) {
            $data['title'] = single_cat_title('', false);
            $data['category'] = 'Catégorie';
            $term = Timber::get_term(new Term());
            $data['termid'] = $term->term_id();
            $query_args = [
                'tax_query' => [
                    [
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $data['termid']
                    ],
                ]
            ];
            $data['posts'] = Timber::get_posts($query_args);
            $countedPosts = $term->count();


        }
        else {
            $countedPosts = wp_count_posts()->publish;
        }

        $data['counted_posts'] = $countedPosts;

        if ( $countedPosts > get_option( 'posts_per_page' ))
        {
            $data['multiplier'] = 1;
        }
        else {
            $data['multiplier'] = 0;
        }

        return new TimberResponse('templates/posts.twig', $data);
    }
}
