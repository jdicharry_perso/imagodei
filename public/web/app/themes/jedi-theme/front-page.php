<?php

/**
 * The Template for the front-page
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Page;
use Rareloop\Lumberjack\Post;

use Timber\Timber;

class FrontPageController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $page = new Page();


        $highlighted_post = $page->meta('front_highlighted_post');
        $context['highlighted_post'] = new Post($highlighted_post);
        $context['event'] = $page->meta('event');
        $is_event_active = $page->meta('event')['is_active'];
        $context['lastposts'] = $this->getLastPosts($is_event_active);
        $context['popular_posts'] = $this->getPopularPosts($is_event_active, $page->meta('popular_posts'));
        $context['block_picto_text'] = $page->meta('block_picto_text');


        return new TimberResponse('templates/front-page.twig', $context);
    }

    private function getPopularPosts(bool $isActive, array $popularPosts )
    {
        $result = [];
        foreach ($popularPosts as $key => $popular_post )
        {
            $result[] = $popular_post;
            if ($isActive === true & $key == 'article_2')
            {
                return $result;
            }
        }
        return $result;

    }
}
