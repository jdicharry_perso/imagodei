<?php

namespace App\Lib\Acf;

class Extended
{
    public function __construct()
    {
        $this->load();
    }

    public function load()
    {
        add_filter('acf/location/rule_types', [$this, 'locationRulesTypes']);
        add_filter('acf/location/rule_values/parent_template', [$this, 'locationRulesValuesParentTemplate']);
        add_filter('acf/location/rule_match/parent_template', [$this, 'locationRulesMatchParentTemplate'], 10, 3);

    }

    public function locationRulesTypes( $choices )
    {

        $choices['Parent']['parent_template'] = 'Parent Template';

        return $choices;

    }

    public function locationRulesValuesParentTemplate( $choices )
    {

        $templates = get_page_templates();

        if ( $templates ) {
            foreach ( $templates as $template_name => $template_filename ) {

                $choices[ $template_filename ] = $template_name;

            }
        }

        return $choices;
    }

    public function locationRulesMatchParentTemplate( $match, $rule, $options )
    {

        $selected_template = $rule['value'];

        global $post;

        if(!$post->post_parent) {
            return false;
        }

        $template = get_page_template_slug( $post->post_parent );

        if( $rule['operator'] == "==" ) {

            $match = ( $selected_template == $template );

        } elseif($rule['operator'] == "!=") {

            $match = ( $selected_template != $template );

        }

        return $match;
    }
}
