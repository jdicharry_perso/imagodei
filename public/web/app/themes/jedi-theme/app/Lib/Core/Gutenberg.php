<?php

namespace App\Lib\Core;

class Gutenberg
{

    private $config;

    public function __construct(Array $config)
    {
        $this->config = $config;
        $this->load();
    }

    public function load()
    {

        add_filter( 'gutenberg_can_edit_post_type', [$this, 'disableGutenberg'], 10, 2 );
        add_filter( 'use_block_editor_for_post_type', [$this, 'disableGutenberg'], 10, 2 );
    }

    /**
     * Templates and Page IDs without editor
     */
    private function disableEditor( $id = false )
    {
        $excluded_templates = $this->config['templates'];

        $excluded_ids = array(
            ($this->config['page_on_front']) ? get_option( 'page_on_front' ) : null,
            ($this->config['page_for_posts']) ? get_option( 'page_for_posts')  : null
        );

        if( empty( $id ) )
            return false;

        $id = intval( $id );
        $template = get_page_template_slug( $id );

        return in_array( $id, $excluded_ids ) || in_array( $template, $excluded_templates );
    }

    /**
     * Disable Gutenberg by template
     */
    public function disableGutenberg( $can_edit, $post_type )
    {
        if( ! ( is_admin() && !empty( $_GET['post'] ) ) )
            return $can_edit;

        if( $this->disableEditor( $_GET['post'] ) )
            $can_edit = false;

        return $can_edit;
    }
}
