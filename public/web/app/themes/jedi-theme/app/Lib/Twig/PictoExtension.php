<?php

namespace App\Lib\Twig;

use manuelodelain\Twig\Extension\SvgExtension;

class PictoExtension extends SvgExtension
{

    public function getName()
    {
        return 'pictoTwigExtension';
    }

    public function getFunctions()
    {
        $parentFunctions = parent::getFunctions();

        // Remplace parent svg function by own
        $parentFunctions[0] = new \Twig_SimpleFunction('picto', array($this, 'getSvg'), array("is_safe" => array("html")));

        return $parentFunctions;
    }

    public function getSvg($path, $params = [])
    {
        $hasClasses = array_key_exists('classes', $params);

        if($hasClasses) {
            $params['classes'] = 'picto ' . $params['classes'];
        } else {
            $params['classes'] = 'picto';
        }

        return parent::getSvg($path, $params);
    }
}
