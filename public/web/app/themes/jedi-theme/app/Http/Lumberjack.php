<?php

namespace App\Http;

use Rareloop\Lumberjack\Http\Lumberjack as LumberjackCore;
use Rareloop\Lumberjack\Post;
use Timber\Term;
use Timber\Timber;

class Lumberjack extends LumberjackCore
{
    public function addToContext($context)
    {
        $context['is_home'] = is_home();
        $context['is_front_page'] = is_front_page();
        $context['is_logged_in'] = is_user_logged_in();
        $context['options'] = get_fields('options');
        $context['home'] = get_permalink( get_option( 'page_on_front' ) );
        $context['archive_url'] = get_permalink( get_option( 'page_for_posts' ) );
        $context['menu_categories'] = $this->getHeaderCategoriesMenu();
        $context['menu_pages'] = $this->getHeaderPagesMenu();
        $context['type_highlighted'] = $context['options']['type_highlighted'];
        if ($context['type_highlighted'] === "Page") {
            $context['highlighted_element'] = new Post($context['options']['highlighted_page']);
        }
        else {
            $context['highlighted_element'] = new Post($context['options']['highlighted_post']);
        }
        $context['menu_footer'] = $this->getFooterMenu();

        return $context;
    }

    private function getHeaderCategoriesMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'main-nav-categories',
            'container' => 'ul',
            'menu_id' => 'header-category-menu',
            'menu_class' => 'header__category-menu hidden-menu'
        ]);
    }
    private function getHeaderPagesMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'main-nav-pages',
            'container' => 'ul',
            'menu_id' => 'header-page-menu',
            'menu_class' => 'header__page-menu hidden-menu'
        ]);
    }

    private function getFooterMenu()
    {
        return wp_nav_menu([
            'echo' => false,
            'theme_location' => 'footer-menu',
            'container' => 'ul',
            'menu_id' => 'footer-menu',
            'menu_class' => 'footer-menu'
        ]);
    }
}
