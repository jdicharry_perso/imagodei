<?php

namespace App\Http\Controllers;

use Timber\Timber;
use Rareloop\Lumberjack\Http\Controller as BaseController;
use Rareloop\Lumberjack\Http\ServerRequest;


class AjaxController extends BaseController
{
    public $context;

    public function showNewSelection( ServerRequest $request )
    {
        // Alterate main wp query increasing the number of posts

        $multiplier = $request->input('multiplier') + 1;
        $counted_posts = $request->input('countedposts');
        $termId = $request->input('termid');
        $posts_per_page = get_option( 'posts_per_page' );
        $total_posts = $multiplier * $posts_per_page;


        if ($counted_posts <= $total_posts)
        {
            $multiplier = 0;
        }
        if ($termId ==! "")
        {

            $query_args = [
                'numberposts'	=> $total_posts,
                'tax_query' => [
                    [
                        'taxonomy' => 'category',
                        'field' => 'term_id',
                        'terms' => $termId
                    ],
                ]
            ];
        }
        else {
            $query_args = [
                'numberposts'	=> $total_posts
            ];
        }

        $data['posts'] = Timber::get_posts($query_args);
        $data['multiplier'] = $multiplier;

        Timber::render(['posts/selection.twig'], $data);
        exit;

    }

}
