<?php

namespace App\Http\Controllers;

use Rareloop\Lumberjack\Http\Controller as BaseController;
use Rareloop\Lumberjack\QueryBuilder;


class Controller extends BaseController
{

    public static function wp_query_vars()
    {
        global $wp_query;
        return $wp_query->query_vars;
    }

    public function getLastPosts(bool $isEvent )
    {
        $currentPostId = null;
        if(is_single()) {
            $currentPostId = get_the_ID();
        }
        $quantity = 3;

        if ($isEvent) {
            $quantity = 2;
        }

        $lastPosts = (new QueryBuilder)->wherePostType('post')
        ->limit($quantity)
        ->whereIdNotIn([$currentPostId])
        ->get();

        return $lastPosts;
    }
}
