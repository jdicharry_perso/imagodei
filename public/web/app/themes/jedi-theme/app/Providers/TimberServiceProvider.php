<?php

namespace App\Providers;

use App\Lib\Utils;
use Rareloop\Lumberjack\Providers\ServiceProvider;
use Timber\PostQuery;

class TimberServiceProvider extends ServiceProvider
{
    /**
     * Perform any required boot operations
     *
     * @return void
     */
    public function boot()
    {
        add_filter('timber/context', [$this, 'updatePostsContext']);
    }

    public function updatePostsContext($context)
    {
        $postTypesClass = [];

        foreach($context['posts'] as $post) {
            if( !array_key_exists($post->post_type, $postTypesClass) ){
                $postTypesClass[$post->post_type] = Utils::getPostClassByType($post->post_type);
            }
        }
        $context['posts'] = new PostQuery(false, $postTypesClass);
        return $context;
    }
}
