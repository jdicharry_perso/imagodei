<?php

namespace App\Providers;

use App\Lib\Core\Gutenberg;
use Rareloop\Lumberjack\Facades\Config;
use Rareloop\Lumberjack\Providers\ServiceProvider;

class GutenbergServiceProvider extends ServiceProvider
{
    /**
     * Register any app specific items into the container
     */
    public function register()
    { }

    /**
     * Perform any additional boot required for this application
     */
    public function boot()
    {
        add_filter('block_categories', [$this, 'mods_block_category'], 10, 2);
        // // disable for posts
        // add_filter('use_block_editor_for_post', '__return_false', 10);

        // // disable for post types
        // add_filter('use_block_editor_for_post_type', '__return_false', 10);
    }

    public function mods_block_category($categories, $post)
    {
        return array_merge(
            $categories,
            [
                [
                    'slug' => 'mods',
                    'title' => __('Modules', 'mods'),
                ],
            ]
        );
    }
}
