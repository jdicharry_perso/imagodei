<?php

/**
 * The Template for displaying all single posts
 */

namespace App;

use App\Http\Controllers\Controller;
use Rareloop\Lumberjack\Http\Responses\TimberResponse;
use Rareloop\Lumberjack\Post;
use Timber\Timber;

class SingleController extends Controller
{
    public function handle()
    {
        $context = Timber::get_context();
        $post = $context['posts'][0];
        $context['terms'] = $post->terms();
        $context['avatar'] = get_avatar(get_the_author_meta('ID'));
        $context['author_link'] = the_author_link();

        $context['post'] = $post;
        $context['lastposts'] = $this->getLastPosts(false);

        $context['title'] = $post->title;
        $context['content'] = $post->content;

        return new TimberResponse('posts/single.twig', $context);
    }
}
