<?php

return [
    /**
     * List of menus to register with WordPress during bootstrap
     */
    'menus' => [
        'main-nav-categories' => __('Menu principal catégories'),
        'main-nav-pages' => __('Menu principal pages'),
        'footer-menu' => __('Menu footer'),
        'categories-menu' => __('Menu liste des articles par catégorie'),

    ],
];
