<?php

return [
    /**
     * List of image sizes to register, each image size looks like:
     *     [
     *         'name' => 'thumb',
     *         'width' => 100,
     *         'height' => 200,
     *         'crop' => true,
     *     ]
     */
    'sizes' => [
        [
            'name' => 'post-card-small',
            'width' => 510,
            'height' => 330,
            'crop' => true,
        ],
        [
            'name' => 'event-card',
            'width' => 340,
            'height' => 925,
            'crop' => true,
        ],
        [
            'name' => 'post-highlight',
            'width' => 800,
            'height' => 500,
            'crop' => true,
        ],
        [
            'name' => 'post-single',
            'width' => 1080,
            'height' => 570,
            'crop' => true,
        ],
    ],
];
