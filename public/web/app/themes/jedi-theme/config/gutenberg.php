<?php
/**
 * Disable gutenberg for templates
 */
return [
    'page_on_front' => true,
    'page_for_posts' => true,
    'templates' => [
        // 'gabarit-6-template.php',
    ]
];
