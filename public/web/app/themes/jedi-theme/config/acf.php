<?php
use App\Http\Gutenberg;

return [
    'type_loading_fields' => 'JSON', // PHP|JSON
    'autoloading' => true,
    'fields' => get_stylesheet_directory() . '/acf-json-fields',
    'block_type' => [
        'Mod txt' => [
            'name'            => 'mod-txt',
            'title'           => __('Bloc de texte', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod txt img' => [
            'name'            => 'mod-txt-img',
            'title'           => __('Bloc de texte et image', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod CTA' => [
            'name'            => 'mod-cta',
            'title'           => __('Bloc call to action', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod pdf' => [
            'name'            => 'mod-pdf',
            'title'           => __('Bloc téléchargement pdf', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
        'Mod embed' => [
            'name'            => 'mod-embed',
            'title'           => __('Bloc vidéo embarquée', 'jedi-theme'),
            'render_callback' => [Gutenberg::class, 'renderMod'],
            'category'        => 'flexibles',
            'icon'            => 'share-alt2',
            'supports'        => ['align' => false]
        ],
    ],
    'options' => [
        'main' => [
            'page_title' => 'Theme settings',
            'menu_title' => 'Theme settings',
            'menu_slug' => 'theme-settings',
            'capability' => 'edit_posts',
            'redirect' => false
        ]
    ]
];
